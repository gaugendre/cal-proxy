# Cal Proxy
Simple Flask app to proxy my school's calendar in order to filter courses I'm not following this year.

## Usage
The app is configurable with environment variable and url parameters.

| Environment variable | URL parameter | Type | Meaning |
| -------------------- | ------------- | ---- | ------- |
| `BASE_URL` | None | String | Original calendar url |
| `COURSES` | `excluded_courses` | Comma separated values, without space | The courses to exclude from the original calendar. Example : `NRP,MGL,TIP` |
| `TYPES` | `excluded_types` | Comma separated values, without space | The course types to exclude from the original calendar. Example : `Cours,TD` |
| `YEAR` | `year` | String | The year to fetch. Example : `4` |
| `GROUP` | `group` | String | The group to fetch. Example : `1` |
| `ALLOWED_YEARS` | None | String | Used to validate year specified in URL params. Example : `3,4,5` |
| `ALLOWED_GROUPS` | None | String | Used to validate group specified in URL params. Example : `1,2,3,4` |

## Example
### Default
After deploying the app and setting the environment variables you want, you can either call the root :
```
http://calendar.example.com/
```
You'll get the default calendar according to your environment.

### URL params
You can also call the url with params :
```
http://calendar.example.com/?year=3&group=2&excluded_types=Cours&excluded_courses=PIT,SIS
```
The url params will override the defaults from your environment and you'll get the calendar for the 3rd year, 2nd group excluding all lectures and all PIT and SIS courses.

# Reuse
If you do reuse my work, please consider linking back to this repository 🙂